// Credit given to:
// https://www.mkyong.com/java8/java-8-foreach-examples/

package com.revature.foreach;

import java.util.*;

public class ForEachExample {
public static void main(String[] args) {
	Map<String, Integer> items = new HashMap<>(); //How does HM differ from HashTable?
	items.put("A", 10);
	items.put("B", 20);
	items.put("C", 30);
	items.put("D", 40);
	items.put("E", 50);
	items.put("F", 60);
	
	//Can't do this! Maps aren't Iterable
	/*for (String item : items) {
		
	}*/
	
	//foreach takes a Lambda expression
	items.forEach((k,v)->System.out.println("Item : " + k + " Count : " + v));
	
	
	ArrayList<String> strings =  new ArrayList<String>();
	strings.add("A");
	strings.add("B");
	strings.add("C");
	strings.add("D");
	strings.add("E");
	
	//Using method reference syntax
	strings.forEach(System.out::println);
}
	
}
